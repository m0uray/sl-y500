# Product Service

# Import framework
from flask import Flask
from flask_restful import Resource, Api
import sys ,os
# Instantiate the app
app = Flask(__name__)
api = Api(app)

import urllib.request

external_ip = urllib.request.urlopen('https://ident.me').read().decode('utf8')
id_docker=open('/root/id_docker').read().splitlines()
print(external_ip)
myhost = os.uname()[1]
lines = open('/root/start_time').read().splitlines()
time_create=lines[0]
class Product(Resource):
    def get(self):
        return {
            'info': [id_docker,external_ip,myhost,time_create]
        }

# Create routes
api.add_resource(Product, '/')

# Run the application
if __name__ == '__main__':

    app.run(host='0.0.0.0', port=80, debug=True)
    print(external_ip)
