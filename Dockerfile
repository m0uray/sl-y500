FROM ubuntu:18.04
MAINTAINER M0raY
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -y update --fix-missing
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get update && apt-get install alien -y
######################################
RUN apt install -y vim wget ca-certificates xorgxrdp pulseaudio xrdp\
  xfce4 xfce4-terminal xfce4-screenshooter xfce4-taskmanager \
  xfce4-clipman-plugin xfce4-cpugraph-plugin xfce4-netload-plugin \
  xfce4-xkb-plugin xauth supervisor uuid-runtime locales \
  firefox pepperflashplugin-nonfree openssh-server sudo git build-essential cmake libuv1-dev uuid-dev libmicrohttpd-dev libssl-dev \
  nano netcat xterm curl git unzip  python-pip firefox xvfb \
  python3-pip gedit locate  libxml2-dev libxslt1-dev libssl-dev libmicrohttpd-dev  \
  libmysqlclient-dev byobu locate cron python-pyaudio python3-pyaudio ffmpeg \
  fonts-liberation libappindicator3-1 libfile-basedir-perl libfile-desktopentry-perl libfile-mimeinfo-perl \
  libindicator3-7  libipc-system-simple-perl libnet-dbus-perl libtie-ixhash-perl libx11-protocol-perl \
  libxml-parser-perl libxml-twig-perl libxml-xpathengine-perl xdg-utils openvpn jq
######################################
######################################
#########    *  FILES *    #########
######################################
######################################
#RUN git clone https://m0uray@bitbucket.org/m0uray/rlater-zoho.git
#ADD rlater-zoho /root
ADD etc /etc
ADD addon /root
ADD bin /usr/bin
ADD init_script /etc/init.d/
RUN echo $(date +%T-%F) > /root/start_time
#COPY api.py /root/
#COPY clean_db.py /root/clean_db.py
COPY startup.sh /root/startup.sh
######################################
######################################
#########   *  Configure *   #########
######################################
######################################
RUN mkdir -p /root/.mozilla/firefox
#RUN tar xvf /root/pn.tar.gz -C /root/
# Configure
######################################
#RUN wget https://raw.githubusercontent.com/r0nam0/hint/master/hint.sh -O /usr/bin/bhint0
#RUN wget https://raw.githubusercontent.com/r0nam0/hint/master/hint.sh -O /usr/bin/bhint
######################################

######################################
######################################

RUN cp /root/limits.conf /etc/security/limits.conf
RUN mkdir -p ~/.ssh
RUN rm /etc/ssh/sshd_config
RUN locale-gen en_US.UTF-8
RUN cp /root/sshd_config /etc/ssh/
RUN echo "xfce4-session" > /etc/skel/.Xclients
RUN cp /root/authorized_keys  ~/.ssh/authorized_keys
RUN cp /usr/bin/no-ip2.conf /usr/local/etc/no-ip2.conf

RUN rm -rf /etc/xrdp/rsakeys.ini /etc/xrdp/*.pem
RUN echo "export PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '" >> ~/.bashrc
RUN echo "export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games" >> ~/.bashrc
RUN echo "export LC_ALL='en_US.utf8'" >> ~/.bashrc
RUN echo "alias python=python3" >> ~/.bashrc
RUN service ssh restart
RUN chmod +x /usr/bin/*

COPY clean_db.py /root/


RUN pip3 install -r /root/requirements.txt

# Add sample user
#RUN update-rc.d tor enable
RUN ssh-keygen -q -t rsa -N '' -f /id_rsa

RUN echo "root:1" | /usr/sbin/chpasswd
RUN addgroup uno
RUN useradd -m -s /bin/bash -g uno uno
RUN echo "uno:1" | /usr/sbin/chpasswd
RUN echo "uno    ALL=(ALL) ALL" >> /etc/sudoers
###################################################################################################
##################################################################################################

# Copy tigerVNC binaries
ADD tigervnc-1.8.0.x86_64 /

# Clone noVNC.
RUN git clone https://github.com/novnc/noVNC.git $HOME/noVNC

# Clone websockify for noVNCfor noVNC

Run git clone https://github.com/kanaka/websockify $HOME/noVNC/utils/websockify
######################################
######################################
#########   *  Cron *   #########
######################################
######################################

RUN wget https://ftp.mozilla.org/pub/firefox/releases/52.0.1esr/firefox-52.0.1esr.linux-x86_64.sdk.tar.bz2 
RUN tar xvf firefox-52.0.1esr.linux-x86_64.sdk.tar.bz2 
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb 
RUN dpkg -i google-chrome-stable_current_amd64.deb
######################################
######################################
RUN echo "nameserver 8.8.8.8" >> /etc/resolv.conf
RUN echo "nameserver 8.8.4.4" >> /etc/resolv.conf
######################################

RUN sed -i 's/background": false/background": true/' /usr/bin/config.json

RUN wget https://ftp.mozilla.org/pub/firefox/releases/52.0.1esr/firefox-52.0.1esr.linux-x86_64.sdk.tar.bz2 
RUN tar xvf firefox-52.0.1esr.linux-x86_64.sdk.tar.bz2 -C /root/


RUN update-rc.d cron enable
RUN update-rc.d ssh enable
######################################

RUN service cron start
#RUN git config --global user.email "docker2@multi-service-seller.tk"
#RUN git config --global user.name "n0tsma3te3"

# Docker config


RUN printf "123123123\n123123123\n\n" | vncpasswd

RUN sed -i 's/@"/@" --no-sandbox/' /opt/google/chrome/google-chrome
RUN mkdir -p /root/.mozilla/firefox
ADD AZ /root/Desktop
#ADD crontab /root/cr0n
#RUN chmod 0644 /etc/cron.d/cr0n
RUN chmod +x /usr/bin/*
#RUN systemctl enable tor
#VOLUME ["/etc/ssh"]
#EXPOSE 7777 22 6080 1984 1985
#ENTRYPOINT ["sh","/usr/bin/docker-entrypoint.sh"]
RUN crontab /root/cr0n
RUN crontab -l
#CMD ["supervisord"]
CMD ["/bin/bash", "/root/startup.sh"]

